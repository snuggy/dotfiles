function! DoRemote(arg)
  UpdateRemotePlugins
endfunction
call plug#begin()
" looking
  Plug 'mhinz/vim-startify'
  Plug 'Yggdroot/indentLine'
  Plug 'ryanoasis/vim-devicons'
  Plug 'myusuf3/numbers.vim'
  Plug 'airblade/vim-gitgutter'
  Plug 'lilydjwg/colorizer'
  Plug 'junegunn/goyo.vim'
  Plug 'yuqio/vim-darkspace'
" completion/templates
  Plug 'jiangmiao/auto-pairs'
  Plug 'ervandew/supertab'
  Plug 'scrooloose/nerdcommenter'
  Plug 'honza/vim-snippets'
  Plug 'tpope/vim-fugitive'
  Plug 'shumphrey/fugitive-gitlab.vim'
" command additions
  Plug 'easymotion/vim-easymotion'
  Plug 'tpope/vim-surround'
  Plug 'junegunn/vim-easy-align'
  Plug 'wellle/targets.vim'
  Plug 'junegunn/fzf'
  Plug 'airblade/vim-rooter'
" utilities
  Plug 'neomake/neomake'
  Plug 'kassio/neoterm'
  Plug 'sjl/gundo.vim'
" documentation
  Plug 'rhysd/nyaovim-markdown-preview'
  Plug 'xolox/vim-notes'
  Plug 'xolox/vim-misc'
  Plug 'itchyny/calendar.vim'
  Plug 'junegunn/vim-journal'
  Plug 'KevinBockelandt/notoire'
" navigation
  Plug 'scrooloose/nerdtree'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'wesleyche/SrcExpl'
  Plug 'majutsushi/tagbar'
  Plug 'rizzatti/dash.vim'
  Plug 'eugen0329/vim-esearch'
  Plug 'wikitopian/hardmode'
" python
  Plug 'zchee/deoplete-jedi'
" html/css/javascript
  Plug 'mattn/emmet-vim'
" R
  Plug 'jalvesaq/nvim-r'
call plug#end()

" Fundamental settings
  set fileencoding=utf-8
  set fileencodings=ucs-bom,utf-8,gbk,cp936,latin-1
  set fileformat=unix
  set fileformats=unix,dos,mac
  filetype on
  filetype plugin on
  filetype plugin indent on
  syntax on
" Some useful settings
  set smartindent
  set expandtab         "tab to spaces
  set tabstop=2         "the width of a tab
  set shiftwidth=2      "the width for indent
  set foldenable
  set foldmethod=indent "folding by indent
  set foldlevel=99
  set ignorecase        "ignore the case when search texts
  set smartcase         "if searching text contains uppercase case will not be ignored
  set spell!
" Lookings
  set number           "line number
  "set cursorline       "highlight the line of the cursor
  "set cursorcolumn     "highlight the column of the cursor
  :colorscheme darkspace
  let g:tex_conceal = ""
  :set wrap
  :set linebreak
  imap ,, <Esc>
  tmap ,, <Esc>
  nnoremap <leader>h <Esc>:call ToggleHardMode()<CR>
  set undofile
  set undodir=~/.nvim/undo

map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_

" Plugin Settings
let g:notoire_folder = '~/Documents/notes/notoire/'
