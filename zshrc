# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob
unsetopt beep nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/fraser/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -v '^?' backward-delete-char

### zstyle 
zstyle :omz:plugins:ssh-agent agent-forwarding on
zstyle :omz:plugins:ssh-agent lifetime

### oh-my-zsh
export ZSH=/usr/share/oh-my-zsh
## termux oh-my-zsh
export ZSH=$HOME/.oh-my-zsh
##Scripts and bin
export PATH=$HOME/.bin:$PATH
for f (~/.scripts/autorun/**/*(N.))  . $f

### Plugins
plugins=(git ssh-agent archlinux colored-man-pages vi-mode tmux)

### Environmental Variables
KITTY_ENABLE_WAYLAND=1
MOZ_ENABLE_WAYLAND=1
_JAVA_AWT_WM_NONREPARENTING=1
RANGER_LOAD_DEFAULT_RC=FALSE
#CALCURSE_CALDAV_PASSWORD=$(pass show calcurse-snugnextcloud) calcurse-caldav

## gpg-agent environmental variable
GPG_TTY=$(tty)
export GPG_TTY
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

### Environmental Aliases
alias android-studio="_JAVA_AWT_WM_NONREPARENTING=1 android-studio"

### Aliases
alias wofiedit="nvim ~/.config/wofi/config"
alias wofieditcss="nvim ~/.config/wofi/style.css"
alias swayedit="nvim ~/.config/sway/config"
alias waybaredit="nvim ~/.config/waybar/config"
alias waybareditcss="nvim ~/.config/waybar/style.css"
alias zshedit="nvim ~/.zshrc"
alias zshreload="source ~/.zshrc && source $ZSH/oh-my-zsh.sh"
alias dmenu_theme="dmenu_run -nb "$color0" -nf "$color15" -sb "$color1" -sf "$color15""
alias kittyedit="nvim ~/.config/kitty/kitty.conf"
alias nvimedit="nvim ~/.config/nvim/init.vim"
alias tmuxedit="nvim ~/.tmux.conf"
alias nmreload="nmcli connection reload"
alias curtinconnect="nmcli connection up student-curtin --ask"
alias curtindisconnect="nmcli connection down student-curtin"
alias eduroamconnect="nmcli connection up eduroam --ask"
alias eduroamdisconnect="nmcli connection down eduroam --ask"
alias night="redshift -m wayland -O 1750 &!"
alias cp="cp -i"
alias mv="mv -i"
alias p1="ping 1.1"
alias screenshot="slurp | grim -g -"
alias weather="curl http://wttr.in"
alias align="column -t"
alias calserv="python3 -m radicale --config "~/.config/radicale/config""

### External Program Aliases
alias cat='bat'
alias ping='prettyping --nolegend'
alias help='tldr'

eval $(ssh-agent) > /dev/null
### Prompt
NEWLINE=$'\n'
PROMPT="%n - %~${NEWLINE}> "

# Change cursor shape for different vi modes.
# Requires ncurses-utils to be installed to change the cursor to being blank
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
    tput cnorm
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
    tput civis
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'lfcd\n'

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

#if which tmux 2>81 </dev/null; then
#  if [ $TERM != "screen-256color" ] && [  $TERM != "screen" ]; then
#    tmux attach -t session || tmux new -s session; exit
#  fi
#fi

# FZF Keybindings
#/usr/share/fzf/key-bindings.zsh
#/usr/share/fzf/completion.zsh
