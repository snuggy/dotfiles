# snug's dotfiles

These are my dotfiles for Sway on Arch, i3 on Proxmox (Debian) and Termux running on an eink tablet.

Termux doesn't use an X / Wayland server, so 'Window Management' is done through tmux, you can check .tmux.conf for the changes, but the most notable is that the prefix key has been changed to Alt-A (+ vim keybinds)

There are some systemd user modules for syncthing and ssh-agent

## Installation
Dotfiles are deployed using dotbot, which relies on python and git. Provided you have them installed, run the following commands and everything will symlink from the .dotfiles directory into .config

```
git clone https://github.com/snug/dotfiles .dotfiles
cd .dotfiles && sh ./install

```

## Programs

```
  zsh
	oh-my-zsh
	neovim
  sway
	kitty
	wofi
	waybar
	redshift-wayland-git
	mako
	brightnessctl
	dmenu
	network-manager-applet
  neofetch
```
